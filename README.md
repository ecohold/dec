# dec

**Deep Energy Cloud**

## 🚀 Visão

O dec é um servidor GraphQL que fornece informações sobre a energia dos servidores deep models. O servidor pode ser usado para monitorar o consumo de energia dos servidores, identificar problemas de consumo de energia e tomar medidas para reduzir o consumo de energia.

**😎 Missão**

O dec tem a missão de ajudar as pessoas e empresas a tomar decisões mais informadas sobre o consumo de energia dos servidores deep models. O servidor fornece informações que podem ser usadas para melhorar a eficiência energética dos servidores e reduzir os custos de energia.

**🔥 Propósito**

O dec tem o propósito de fornecer informações sobre o consumo de energia dos servidores deep models de forma acessível e fácil de usar. O servidor é uma ferramenta que pode ser usada por qualquer pessoa, independentemente de sua experiência técnica.

**🥰 Valores**

* **Acessibilidade:** O dec é uma ferramenta acessível para todos. Não é necessário ter experiência técnica para usar o dec.
* **Transparência:** O dec fornece informações sobre o consumo de energia dos servidores deep models de forma transparente. Os usuários podem ver os valores de consumo de energia em tempo real.
* **Relevância:** As informações fornecidas pelo dec são relevantes para os usuários. As informações podem ser usadas para tomar decisões sobre o consumo de energia dos servidores.
* **Precisão:** As informações fornecidas pelo dec são precisas. Os valores de consumo de energia são atualizados em tempo real.
* **Confiabilidade:** O dec é uma ferramenta confiável. O servidor está disponível 24 horas por dia, 7 dias por semana.

## 😍 Como usar

O dec pode ser usado através da URL `/graphql`. Para fazer uma consulta ao dec, você deve usar a linguagem GraphQL.

```
query {
  servers {
    server {
      nome
      modelo
      mac
      ip
      configuracao
      redundancia
      kwh_em_uso
      kwh_nominal
      kwh_maxima
      cpus
      temperatura_em_uso
      temperatura_nominal
      temperatura_maxima
      temperatura_deep_overclock
      nucleos
      temperatura_atual
      temperatura_nominal
      temperatura_maxima
      temperatura_deep_overclock
      memoria
      temperatura_mem_atual
      temperatura_mem_nominal
      temperatura_mem_maxima
      temperatura_mem_deep_overclock
      num_usuario_atual
      num_usuario_minimo
      num_usuario_nominal
      num_usuario_maximo
      vms_quantidade
      vms_frequencia
      trafico_dados
      pid_minimo
      pid_nominal
      pid_maximo
      temperatura_gabinete
      porta_teste
      url_teste
    }
  }
}
```

Esta consulta retorna uma lista de servidores, incluindo o nome, modelo, mac, ip, configuração, redundancia, consumo de energia, temperatura e outros dados.

Para obter mais informações sobre como usar o servidor GraphQL, consulte a documentação: https://graphql.org/learn/

## 🔥 Exemplos de consultas

Aqui estão alguns exemplos de consultas que podem ser feitas ao dec:

* Obter uma lista de todos os servidores:

```
query {
  servers {
    id
    name
  }
}
```

* Obter informações sobre um servidor específico:

```
query {
  server(id: "123456") {
    name
    model
    mac
    ip
  }
}
```

* Obter uma lista de servidores que estão com consumo de energia acima da média:

```
query {
  servers(where: {
    kwh_em_uso: { gt: 100 }
  }) {
    name
    kwh_em_uso
  }
}
```

## 😎 ODS 13

O dec contribui para o ODS 13, que visa **combater as mudanças climáticas**. O dec ajuda a reduzir o consumo de energia dos servidores, o que contribui para a redução das emissões de gases de efeito estufa e zerar o aquecimento global.

O dec é uma ferramenta para ser usada em qualquer servidor, extensão browser, desktop, app, raspi, na fabrica, no equipamento usado, no data center, por qualquer pessoa ou criança sem experiência técnica. O dec ajuda as pessoas, micro e pequenas empresas, startup´s, decacórnios, mega empresas, governos, instituições e negócios a tomarem decisões mais informadas sobre boas práticas noo consumo inteligente de energia

[![ODS 13](https://ods.pt/wp-content/uploads/2022/08/13.png)](https://i.imgur.com/123456789.png)

