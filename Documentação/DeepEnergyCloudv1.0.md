# Projeto Deep Energy Clouds 
* versão v1.0 

## Autores

* Zeh Sobrinho
* Bard

## Data

2023-08-13



## Sumário

Este documento descreve o projeto Deep Energy Clouds, que é uma plataforma distribuída para coletar e disponibilizar dados de energia. O projeto será implementado em 12 instâncias, sendo duas por região/zona, ou seja, redundância intercontinental. Cada instância rodará a mesma aplicação DEC, que coletará e disponibilizará dados de si mesma e os publicará em uma URL pública GraphQL. Se uma instância falhar, outra instância assumirá sua função de coletar e distribuir seus próprios dados. Quando a instância que caiu voltar, ela assumirá o snapshot de quem assumiu seu lugar.

## Requisitos

O projeto Deep Energy Clouds tem os seguintes requisitos:

* 12 instâncias, sendo duas por região/zona
* Aplicativo DEC rodando em cada instância
* Coleta e disponibilização de dados de si mesmo
* Publicação de dados em uma URL pública GraphQL
* Redundância de instâncias

## Design

O projeto Deep Energy Clouds será implementado usando as seguintes tecnologias:

* Google Cloud Platform
* Kubernetes
* Helm
* Prometheus
* Grafana

O projeto será projetado para ser altamente escalável e resiliente. As instâncias serão distribuídas por toda a rede do Google Cloud Platform, o que garantirá que o aplicativo esteja sempre disponível, mesmo que uma região ou zona falhe. O aplicativo DEC será projetado para ser altamente tolerante a falhas, o que significa que ele poderá continuar a funcionar mesmo que uma instância falhe.

## Implementação

O projeto Deep Energy Clouds será implementado da seguinte forma:

1. Crie 12 instâncias no Google Cloud Platform.
2. Configure cada instância para executar o aplicativo DEC.
3. Configure o aplicativo DEC para coletar e disponibilizar dados de si mesmo.
4. Publique os dados do aplicativo DEC em uma URL pública GraphQL.
5. Configure a redundância de instâncias.

## Teste

O projeto Deep Energy Clouds será testado da seguinte forma:

1. Teste o aplicativo DEC individualmente.
2. Teste o aplicativo DEC em conjunto.
3. Teste a redundância de instâncias.

## Implantação

O projeto Deep Energy Clouds será implantado da seguinte forma:

1. Implemente o aplicativo DEC nas instâncias.
2. Implemente a URL pública GraphQL.
3. Configure a redundância de instâncias.

## Manutenção

O projeto Deep Energy Clouds será mantido da seguinte forma:

1. Monitore o aplicativo DEC para verificar se há problemas.
2. Faça atualizações no aplicativo DEC conforme necessário.
3. Faça atualizações nas instâncias conforme necessário.

## Documentação

O projeto Deep Energy Clouds será documentado da seguinte forma:

1. Crie um manual do usuário para o aplicativo DEC.
2. Crie um guia de implantação para o aplicativo DEC.
3. Crie um guia de manutenção para o aplicativo DEC.

## Desenvolvimento futuro

O projeto Deep Energy Clouds tem os seguintes planos de desenvolvimento futuro:

* Adicionar novos recursos ao aplicativo DEC.
* Expandir a rede de instâncias.
* Melhorar a redundância de instâncias.
* Automatizar o processo de implantação.
* Melhorar a documentação.



Espero que isso ajude!
